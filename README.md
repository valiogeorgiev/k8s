
# Set up K8s cluster on deb based OSs using kubeadm (v1.28+)

### Install CRI-O, kubeadm, kubelet and kubectl:
```
### Install some prereqs
apt update && apt install -y apt-transport-https ca-certificates curl gpg sudo

### Add the Kubernetes repo
mkdir /etc/apt/keyrings/ || echo "Folder already exist"
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.28/deb/Release.key | gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
echo "deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.28/deb/ /" | tee /etc/apt/sources.list.d/kubernetes.list

### Add CRI-O repo
curl -fsSL https://pkgs.k8s.io/addons:/cri-o:/prerelease:/main/deb/Release.key |
    gpg --dearmor -o /etc/apt/keyrings/cri-o-apt-keyring.gpg
echo "deb [signed-by=/etc/apt/keyrings/cri-o-apt-keyring.gpg] https://pkgs.k8s.io/addons:/cri-o:/prerelease:/main/deb/ /" |
    tee /etc/apt/sources.list.d/cri-o.list
   
apt-get update && apt-get install -y cri-o kubelet kubeadm kubectl
systemctl start crio.service
ls -la /run/crio/crio.sock

### Enable ip forwarding and load br_netfilter module
cat <<EOF | tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF

modprobe overlay
modprobe br_netfilter

### sysctl params required by setup, params persist across reboots
cat <<EOF | tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF

### Apply sysctl params without reboot
sysctl --system

### Disable swap
swapoff -va
sed -i '/ swap / s/^/#/' /etc/fstab
```

### Initialize K8s Master:
```
kubeadm init --pod-network-cidr=10.244.0.0/16
```

### Set up CNI

Flannel is good for simple networking, without thinking too much of network policies and security.
- Flannel [install guide](https://github.com/flannel-io/flannel) (execute only on the master, not on worker nodes)

Calico is more advanced, provide network policies and enterprise support. 
- Calico [install guide](https://docs.tigera.io/calico/latest/getting-started/kubernetes/self-managed-onprem/onpremises)


### Add a Worker Node (get command details from K8s master)
```
# Run this one the master node
kubeadm token create --print-join-command

# Then run this on the worker node who will join the cluster
kubeadm join <Control-Plane-IP-Address>:6443 --token xxxxxxxxxxxxxx \
       	--discovery-token-ca-cert-hash sha256:f32357bd4xxxxxx....
```

### Add useful alias
```
echo 'alias k=kubectl' >> $HOME/.bashrc

### Set up default kubectl profile access
mkdir $HOME/.kube
cp -av /etc/kubernetes/kubelet.conf $HOME/.kube/config
```

### Create Namespaces
```
kubectl create ns prod
kubectl create ns dev
```
### Create Users
You can use the output to create a new `$HOME/.kube/config` file and access the K8s cluster as user John.\
Then create a role with appropriate permissions and assign the user to it with the help of a RoleBinding object.
```
kubeadm kubeconfig user --client-name=john
```

### Create Roles
```
kubectl apply -f https://gitlab.com/valiogeorgiev/k8s/-/raw/main/roles/ns-dev--role-pod-reader.yaml
kubectl apply -f https://gitlab.com/valiogeorgiev/k8s/-/raw/main/roles/ns-dev--role-dev-admin.yaml
```

### Create RoleBindings
```
kubectl apply -f https://gitlab.com/valiogeorgiev/k8s/-/raw/main/rolebindings/ns-dev--rolebinding-john--pod-reader.yaml
```

### Create Secrets
``` 
kubectl create secret docker-registry regcred --docker-server=https://index.docker.io/v1/ \
--docker-username=<USER> --docker-password='<PASS>' --docker-email=<EMAIL> -n <NAMESPACE>
```
Then, one can use it in deployment/pod files like this:
``` 
apiVersion: apps/v1
kind: Deployment
...
    spec:
      containers:
      - name: nginx
        image: nginx:1.14.2
        ports:
        - containerPort: 80
      imagePullSecrets:
      - name: regcred
```

### Create Deployments
```
kubectl apply -f https://gitlab.com/valiogeorgiev/k8s/-/raw/main/deployments/deployment-nginx.yaml
```

### Set Resource Limits
To easily see usage, install Metrics server -> https://github.com/kubernetes-sigs/metrics-server





### Upgrade K8s Cluster 


### Network Policies
https://kubernetes.io/docs/concepts/services-networking/network-policies/
One might want to check Calico CNI as well -> https://www.tigera.io/project-calico
 
### Hardening K8s Cluster
K8s official docs:
https://kubernetes.io/docs/concepts/security/

Check out NSA Kubernetes Hardening Guide:
https://media.defense.gov/2022/Aug/29/2003066362/-1/-1/0/CTR_KUBERNETES_HARDENING_GUIDANCE_1.2_20220829.PDF

Kubernetes Pentesting:
https://cloud.hacktricks.xyz/pentesting-cloud/kubernetes-security

### Troubleshooting K8s Cluster 

K8s node is in status "Not Ready":
1. Check if /etc/kubernetes/kubelet.conf file is valid and the certs are part of the main CA
2. If not, delete this file, also delete all cert file here /var/lib/kubelet/pki/*
3. Execute: kubeadm kubeconfig user --client-name system:node:<NODE HOSTNAME> --org system:nodes > kubelet.conf
4. Restart kubelet service and validate the newly created certs in /var/lib/kubelet/pki/ folder.
